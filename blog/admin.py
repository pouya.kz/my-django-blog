from django.contrib import admin, messages
from django.utils.translation import ngettext
from .models import Article, Category

# Change Admin Header
admin.site.site_header = "مدیریت سایت پویا"

# Register your models here.
@admin.action(description='انتشار مقالات انتخاب شده')
def make_published_article(modeladmin, request, queryset):
    updated = queryset.update(status='p')
    modeladmin.message_user(request, ngettext(
            '%d مقاله منتشر شد.',
            '%d مقاله منتشر شدند.',
            updated,
        ) % updated, messages.SUCCESS)

@admin.action(description='پیش‌نویس شدن مقالات انتخاب شده')
def make_draft_article(modeladmin, request, queryset):
    updated = queryset.update(status='d')
    modeladmin.message_user(request, ngettext(
            '%d مقاله پیش‌نویس شد.',
            '%d مقاله پیش‌نویش شدند.',
            updated,
        ) % updated, messages.SUCCESS)


@admin.action(description='انتشار دسته‌بندی انتخاب شده')
def make_published_cateegory(modeladmin, request, queryset):
    updated = queryset.update(status=True)
    modeladmin.message_user(request, ngettext(
            '%d دسته‌بندی منتشر شد.',
            '%d دسته‌بندی منتشر شدند.',
            updated,
        ) % updated, messages.SUCCESS)

@admin.action(description='پیش‌نویس شدن دسته‌بندی انتخاب شده')
def make_draft_category(modeladmin, request, queryset):
    updated = queryset.update(status=False)
    modeladmin.message_user(request, ngettext(
            '%d دسته‌بندی پیش‌نویس شد.',
            '%d دسته‌بندی پیش‌نویش شدند.',
            updated,
        ) % updated, messages.SUCCESS)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position','title', 'slug', 'parent', 'status')
    list_filter =  (['status'])
    search_fields = ['title', 'slug']
    prepopulated_fields = {'slug': ('title',)}
    actions = [make_published_cateegory, make_draft_category]

admin.site.register(Category, CategoryAdmin)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumbnail_tag', 'slug', 'author', 'jpublish', 'is_special', 'status', 'category_to_str')
    list_filter =  ( 'publish', 'status', 'author')
    search_fields = ['title', 'description']
    prepopulated_fields = {'slug': ('title',)}
    ordering = ['status', '-publish']
    actions = [make_published_article, make_draft_article]



admin.site.register(Article, ArticleAdmin)


