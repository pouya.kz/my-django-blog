# Generated by Django 4.0.4 on 2022-07-04 12:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_article_is_special_alter_article_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='status',
            field=models.CharField(choices=[('i', 'در حال بررسی'), ('d', 'پیش\u200cنویس'), ('p', 'منتشر شده'), ('b', 'برگشت داده شده')], max_length=1, verbose_name='وضعیت'),
        ),
    ]
