# Generated by Django 4.0.4 on 2022-06-27 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_alter_article_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='is_special',
            field=models.BooleanField(default=False, verbose_name='مقاله ویژه'),
        ),
        migrations.AlterField(
            model_name='article',
            name='status',
            field=models.CharField(choices=[('b', 'برگشت داده شده'), ('i', 'در حال بررسی'), ('p', 'منتشر شده'), ('d', 'پیش\u200cنویس')], max_length=1, verbose_name='وضعیت'),
        ),
    ]
